﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtMouse : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    private void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        /*
        if (Physics.Raycast(ray,out hit))
        {
            if (Vector3.Distance(hit.point, transform.position) > 0.1f)
                if (hit.transform.tag == "Ground")
                    transform.LookAt(hit.point);

        }
        */

        
        // De esta forma lo interesante es que podes tener control de todas RaycastHit y si queres saltearte el collider de la unit
        RaycastHit[] hits = Physics.RaycastAll(ray);
        foreach (RaycastHit hit in hits)
        {
            if (hit.transform.tag == "Ground")
                transform.LookAt(hit.point);
        }
        
    }

}
