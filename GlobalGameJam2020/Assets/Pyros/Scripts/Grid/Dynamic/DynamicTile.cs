﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DynamicTile : MonoBehaviour, IComparer
{
    [HideInInspector]
    public new string name;

    public static  int IdCount;

    public int id;

    public Vector2Int coordinates;

    [HideInInspector]
    public Vector2 xyPosition;

    public Adjacencies adjacencies;


    /// <summary>
    /// All the entities contained within the Tile.
    /// </summary>
    public List<GridEntity> entitiesInsideMy; 


    public void SetTile(int xCoord, int yCoord)
    {
        SetTile(xCoord, yCoord, Vector2.one);
    }

    public void SetTile(int xCoord, int yCoord, Vector2 offSetPosition)
    {
        coordinates = new Vector2Int(xCoord, yCoord);
        xyPosition = new Vector2(coordinates.x * offSetPosition.x, coordinates.y * offSetPosition.y);

        name = "Tile " + "(" + xCoord + "," + yCoord + ")";
        id = DynamicTile.IdCount;
        IdCount++;
    }

    /// <summary>
    /// This just checks if there are other tiles arround, if there is then updates the adjacencies mine an theirs.
    /// </summary>
    /// <param name="matrix"></param>
    public void CheckForAdjacencies(ref List<DynamicTile> matrix)
    {/*
        foreach (DynamicTile tile in matrix)
        {
            if ((tile.coordinates - coordinates) == Vector2Int.Up())
            {
                tile.adjacencies.down = this;
                adjacencies.up = tile;
            }
            if ((tile.coordinates - coordinates) == Vector2Int.Left())
            {
                tile.adjacencies.right = this;
                adjacencies.left = tile;
            }
            if ((tile.coordinates - coordinates) == Vector2Int.Down())
            {
                tile.adjacencies.up = this;
                adjacencies.down = tile;
            }
            if ((tile.coordinates - coordinates) == Vector2Int.Right())
            {
                tile.adjacencies.left = this;
                adjacencies.right = tile;
            }
        }
        */
    }

    public void AddEntity(GridEntity entity)
    {
        entitiesInsideMy.Add(entity);
    }

    public void RemoveEntity(GridEntity entity)
    {
        entitiesInsideMy.Remove(entity);
    }

    public int Compare(object x, object y)
    {
        throw new System.NotImplementedException();
    }

    [ContextMenu("Test")]
    public void Test()
    {
        Debug.Log(adjacencies.up != null ? "Tengo adyacencia arriba" : "No tengo nada arriba mio");
        Debug.Log(adjacencies.left != null ? "Tengo adyacencia a mi izquierda" : "No tengo nada a mi izquierda");
        Debug.Log(adjacencies.down != null ? "Tengo adyacencia abajo" : "No tengo nada abajo mio");
        Debug.Log(adjacencies.right != null ? "Tengo adyacencia a la derecha" : "No tengo nada a mi derecha");
    }

}


