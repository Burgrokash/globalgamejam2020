﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pyros;

[RequireComponent(typeof(Pool))]
public class Grid : MonoBehaviour
{
    public static Grid control;

    /// <summary>
    /// It manages everything related to entities on the grid.
    /// </summary>
    public GridEntitesManager entitiesManager;

    [Header("Grid Settings")]
    /// <summary>
    /// The x and y offSet of the Tiles
    /// </summary>
    [SerializeField]
    [Tooltip("The X and Y offSet of the Tiles")]
    private Vector2 offSet;

    /// <summary>
    /// The gameObject used for creating the "Abstract" matrix element. 
    /// </summary>
    public Pool gridElement;

    /// <summary>
    /// The dimensions expresed in ZxZ of the matrix. 
    /// </summary>
    [SerializeField]
    private Vector2Int size;

    [Space]

    /// <summary>
    /// The actual grid that contains all the GridElements forming the abstract structure.
    /// </summary>
    public GridElement[,] grid;

    /*
    /// <summary>
    /// Its a matrix that stores all the tiles on the grid. The advantage of using this dual tile and element structure is that it may give us the
    /// oportunity to create a "worldbuilder".
    /// </summary>
    public Tile[,] tiles;
    */

    public List<Vector2Int> existingTilesOnTheMatrix;

    private void Awake()
    {
        if (control == null)
            control = this;
        else
            Destroy(control);
    }

    private void Start()
    {
        InitializeGrid(size);
    }

    public void InitializeGrid(Vector2Int _size)
    {
        grid = new GridElement[_size.x, _size.y];

        for (int i = 0; i < _size.x; i++)
            for (int j = 0; j < _size.y; j++)
                CreateElement(i, j);
    }

    public void CreateElement(int x, int y)
    {
        GridElement pooledElement;

        pooledElement = gridElement.GetPooledObject().GetComponent<GridElement>();

        pooledElement.SetPosition(x, y, offSet);

        grid[x, y] = pooledElement;

        existingTilesOnTheMatrix.Add(new Vector2Int(x, y));

        // No se si es necesario ahora mismo.
        //pooledTile.CheckForAdjacencies(ref matrix);
    }

  
    [ContextMenu("TestCreateTIle")]
    public void TestCreateTileOnRandom()
    {
        AddTile(MatrixHandler.GetMatrixCenter(size), GetNewTile());
    }

    public Tile GetNewTile()
    {
        return gridElement.GetPooledObject(1).GetComponent<Tile>();
    }

    public void DestroyTile(DynamicTile tileToDestroy)
    {

    }

    public bool AddTile(Vector2Int gridCoordinatesForTile, Tile tile)
    {
        if (!grid[gridCoordinatesForTile.x, gridCoordinatesForTile.y].isOcupied)
            return grid[gridCoordinatesForTile.x, gridCoordinatesForTile.y].AddTile(tile);
        else
        {
            // This is just in case. Prevents an error if someone tries to create a Tile without asking if the GridElement is available.
            Destroy(tile.gameObject);
            return false;
        }
    }
    
    public void AddTilesOnCenter(Vector2Int AmountOfTiles)
    {

    }

    /// <summary>
    /// Returns a desired tile with x and y coordinates, if doesn't exist then returns null.
    /// </summary>
    /// <returns></returns>
    public Tile GetTile(Vector2Int coordiantes)
    {
        if (MatrixHandler.IndexExist(grid,coordiantes))
        {
            if (grid[coordiantes.x, coordiantes.y].tile)
                return grid[coordiantes.x, coordiantes.y].tile;
        }

        return null;
    }
}
