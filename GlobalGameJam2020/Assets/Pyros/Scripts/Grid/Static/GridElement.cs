﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The "Abstract" element used to fill the grid.
/// </summary>
public class GridElement : MonoBehaviour
{
    public Vector2Int coordinates;

    [HideInInspector]
    public Vector2 xzPosition;

    public bool isOcupied;

    /// <summary>
    /// The tile that is located in this element if any.
    /// </summary>
    public Tile tile;

    public void SetPosition(int xCoord, int yCoord)
    {
        SetPosition(xCoord, yCoord, Vector2.one);
    }

    public void SetPosition(int xCoord, int zCoord, Vector2 offSetPosition)
    {
        coordinates = new Vector2Int(xCoord, zCoord);
        xzPosition = new Vector2(coordinates.x * offSetPosition.x, coordinates.y * offSetPosition.y);

        transform.position = new Vector3(xzPosition.x, 0f, xzPosition.y);
    }

    [ContextMenu("TestAddTile")]
    public void TestAddTile()
    {
        if (!isOcupied)
            AddTile(Grid.control.GetNewTile());
    }

    public bool AddTile(Tile tileToAdd)
    {
        if (isOcupied)
            return false;
        else
        {
            tileToAdd.SetTile(this.coordinates.x, this.coordinates.y, xzPosition);
            tile = tileToAdd;
            isOcupied = true;

            // No me gusta que este aca.
            tileToAdd.CheckForAdjacencies();
            return isOcupied;
        }
    }

}
