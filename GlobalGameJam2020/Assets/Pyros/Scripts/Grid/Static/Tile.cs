﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pyros;

[System.Serializable]
public class Tile : MonoBehaviour, IPoolable
{
    [HideInInspector]
    public new string name;

    public static int IdCount;

    public int id;

    public bool walkable;

    public Vector2Int coordinates;

    /// <summary>
    /// Remember that the x and z are used because of the convention of 3D coordinates, so it matches with the flor plane.
    /// </summary>
    [HideInInspector]
    public Vector2 xzPosition;

    public Adjacencies adjacencies;

    /// <summary>
    /// All the entities contained within the Tile.
    /// </summary>
    public List<GridEntity> entitiesInsideMy;

    public void SetTile(int xCoord, int yCoord)
    {
        SetTile(xCoord, yCoord, Vector2.one);
    }

    public void SetTile(int xCoord, int zCoord, Vector2 offSetPosition)
    {
        coordinates = new Vector2Int(xCoord, zCoord);
        xzPosition = new Vector2(offSetPosition.x, offSetPosition.y);

        transform.position = new Vector3(xzPosition.x,0f, xzPosition.y);

        name = "Tile " + "(" + xCoord + "," + zCoord + ")";
        id = Tile.IdCount;
        IdCount++;
    }

    /// <summary>
    /// This just checks if there are other tiles arround, if there is then updates the adjacencies mine an theirs.
    /// </summary>
    /// <param name="matrix"></param>
    public void CheckForAdjacencies()
    {
        Vector2Int indexHelper;

        indexHelper = MatrixHandler.GetUpIndex(Grid.control.grid, coordinates);
        if (indexHelper != null && Grid.control.grid[indexHelper.x, indexHelper.y].isOcupied)
        {
            adjacencies.up = Grid.control.grid[indexHelper.x, indexHelper.y].tile;
            Grid.control.grid[indexHelper.x, indexHelper.y].tile.adjacencies.down = this; 
        }

        indexHelper = MatrixHandler.GetLeftIndex(Grid.control.grid, coordinates);
        if (indexHelper != null && Grid.control.grid[indexHelper.x, indexHelper.y].isOcupied)
        {
            adjacencies.left = Grid.control.grid[indexHelper.x, indexHelper.y].tile;
            Grid.control.grid[indexHelper.x, indexHelper.y].tile.adjacencies.right = this;
        }

        indexHelper = MatrixHandler.GetDownIndex(Grid.control.grid, coordinates);
        if (indexHelper != null && Grid.control.grid[indexHelper.x, indexHelper.y].isOcupied)
        {
            adjacencies.down = Grid.control.grid[indexHelper.x, indexHelper.y].tile;
            Grid.control.grid[indexHelper.x, indexHelper.y].tile.adjacencies.up = this;
        }

        indexHelper = MatrixHandler.GetRightIndex(Grid.control.grid, coordinates);
        if (indexHelper != null && Grid.control.grid[indexHelper.x, indexHelper.y].isOcupied)
        {
            adjacencies.right = Grid.control.grid[indexHelper.x, indexHelper.y].tile;
            Grid.control.grid[indexHelper.x, indexHelper.y].tile.adjacencies.left = this;
        }

    }

    [ContextMenu("AddEntity")]
    public void AddEntityTest()
    {
        Grid.control.entitiesManager.CreateEntity(this);
    }

    public void AddEntity(GridEntity entity)
    {
        entitiesInsideMy.Add(entity);
    }

    public void RemoveEntity(GridEntity entity)
    {
        entitiesInsideMy.Remove(entity);
    }

    public void Initialize()
    {
        Debug.Log("Oli, espawne");
    }
}

[System.Serializable]
public struct Adjacencies
{
    public Tile up;
    public Tile left;
    public Tile down;
    public Tile right;
}