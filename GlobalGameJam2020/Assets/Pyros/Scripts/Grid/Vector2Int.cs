﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Vector2Int
{
    public int x;
    public int y;

    public Vector2Int()
    {
        x = 0;
        y = 0;
    }

    public Vector2Int(int xValue, int yValue)
    {
        x = xValue;
        y = yValue;
    }

    public static Vector2Int Addition(Vector2Int vector1, Vector2Int vector2)
    {
        return new Vector2Int(vector1.x + vector2.x, vector1.y + vector2.y);
    }

    public static Vector2Int Up()
    {
        return new Vector2Int(0, 1);
    }

    public static Vector2Int Left()
    {
        return new Vector2Int(-1, 0);
    }

    public static Vector2Int Down()
    {
        return new Vector2Int(0, -1);
    }

    public static Vector2Int Right()
    {
        return new Vector2Int(1, 0);
    }

    public static Vector2Int Zero()
    {
        return new Vector2Int(0, 0);
    }

    public static Vector2Int Null()
    {
        return new Vector2Int(-1, -1);
    }

    public static float Magnitud(Vector2Int vector)
    {
        return Mathf.Sqrt(Mathf.Pow(vector.x, 2) + Mathf.Pow(vector.y, 2));
    }

    public static Vector2Int operator +(Vector2Int v1, Vector2Int v2)
    {
        return new Vector2Int(v1.x + v2.x, v1.y + v2.y);
    }

    public static Vector2Int operator -(Vector2Int v1, Vector2Int v2)
    {
        return new Vector2Int(v1.x - v2.x, v1.y - v2.y);
    }

    public static bool operator !=(Vector2Int v1, Vector2Int v2)
    {
        return !(v1 == v2);
    }

    public static bool operator ==(Vector2Int v1, Vector2Int v2)
    {
        // If left hand side is null...
        if (ReferenceEquals(v1, null))
        {
            // ...and right hand side is null...
            if (ReferenceEquals(v2, null))
            {
                //...both are null and are Equal.
                return true;
            }

            // ...right hand side is not null, therefore not Equal.
            return false;
        }

        // Return true if the fields match:
        return v1.Equals(v2);
    }

    public override bool Equals(object obj)
    {
        var @int = obj as Vector2Int;
        return @int != null &&
               x == @int.x &&
               y == @int.y;
    }

    public override int GetHashCode()
    {
        var hashCode = 1502939027;
        hashCode = hashCode * -1521134295 + x.GetHashCode();
        hashCode = hashCode * -1521134295 + y.GetHashCode();
        return hashCode;
    }
}
