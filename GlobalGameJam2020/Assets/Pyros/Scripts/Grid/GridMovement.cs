﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Pyros;

/// <summary>
/// Used for Entity movement through the grid.
/// </summary>
public class GridMovement : MonoBehaviour, IPoolable
{
    public GridMovementType manageType;

    public EasingFunction.Ease movementType;
    private EasingFunction.Function function;

    public GridEntity entity;

    private Tile destinyTile;

    [SerializeField]
    private bool loop;

    [SerializeField]
    private bool rewindOnEnd;

    [SerializeField]
    private float elapsedTime;

    [SerializeField]
    private float movementDuration;

    [SerializeField]
    private Transform[] positions;
    private Vector3[] auxPositions;

    [Header("Clamp")]
    [SerializeField]
    private bool xAxis;

    [SerializeField]
    private bool yAxis;

    [SerializeField]
    private bool zAxis;

    [Space]

    public UnityEvent OnStart;
    public UnityEvent OnNewPosition;
    public UnityEvent OnEnd;

    private void Update()
    {
        if (manageType != GridMovementType.WASD)
            return;

        if (Input.GetKeyDown(KeyCode.W))
            InitializeMove(MoveDirection.Up);
        if (Input.GetKeyDown(KeyCode.A))
            InitializeMove(MoveDirection.Left);
        if (Input.GetKeyDown(KeyCode.S))
            InitializeMove(MoveDirection.Down);
        if (Input.GetKeyDown(KeyCode.D))
            InitializeMove(MoveDirection.Right);
    }

    public void SetEntityPosition(GridEntity entity, Tile tile)
    {
        entity.transform.position = new Vector3(tile.xzPosition.x, entity.hight, tile.xzPosition.y);
    }

    public void InitializeMove(MoveDirection direction)
    {
        Vector3[] path = new Vector3[2];
        path[0] = new Vector3(entity.myActualTile.xzPosition.x, entity.hight, entity.myActualTile.xzPosition.y);

        destinyTile = null;

        // Esto hay que revisarlo, no tiene sentido usar un metodo de singleton y usarse como parametro.
        switch (direction)
        {
            case MoveDirection.Up:
                destinyTile = Grid.control.GetTile(MatrixHandler.GetUpIndex(Grid.control.grid, entity.myActualTile.coordinates));
                break;
            case MoveDirection.Left:
                destinyTile = Grid.control.GetTile(MatrixHandler.GetLeftIndex(Grid.control.grid, entity.myActualTile.coordinates));
                break;
            case MoveDirection.Down:
                destinyTile = Grid.control.GetTile(MatrixHandler.GetDownIndex(Grid.control.grid, entity.myActualTile.coordinates));
                break;
            case MoveDirection.Right:
                destinyTile = Grid.control.GetTile(MatrixHandler.GetRightIndex(Grid.control.grid, entity.myActualTile.coordinates));
                break;
            default:
                break;
        }

        path[1] = new Vector3(destinyTile.xzPosition.x, entity.hight, destinyTile.xzPosition.y);
        StartCoroutine(Move(path, movementDuration));
    }

    IEnumerator Move(Vector3[] positions, float duration)
    {
        function = EasingFunction.GetEasingFunction(movementType);

        elapsedTime = 0f;

        float x, y, z, distancePercentage, durationPercentage;
        float totalDistance = GetTotalDistance(positions);

        do
        {
            OnStart.Invoke();

            transform.position = positions[0];

            for (int i = 0; i < positions.Length - 1; i++)
            {
                x = positions[i].x;
                y = positions[i].y;
                z = positions[i].z;

                distancePercentage = Vector3.Distance(positions[i], positions[i + 1]) / totalDistance;
                durationPercentage = duration * distancePercentage;

                while (elapsedTime < durationPercentage)
                {
                    x = function(positions[i].x, positions[i + 1].x, (elapsedTime / durationPercentage));
                    y = function(positions[i].y, positions[i + 1].y, (elapsedTime / durationPercentage));
                    z = function(positions[i].z, positions[i + 1].z, (elapsedTime / durationPercentage));

                    elapsedTime += Time.deltaTime;

                    if (xAxis)
                        x = transform.position.x;
                    if (yAxis)
                        y = transform.position.y;
                    if (zAxis)
                        z = transform.position.z;

                    transform.position = new Vector3(x, y, z);

                    yield return null;
                }

                if (!xAxis && !yAxis && !zAxis)
                    transform.position = positions[i + 1];

                // Feo que este aca y asi.
                entity.SetActualTile(destinyTile);

                elapsedTime = 0f;

                if (i < positions.Length - 2)
                    OnNewPosition.Invoke();
            }

            if (!rewindOnEnd)
                OnEnd.Invoke();
            else
            {
                for (int i = positions.Length - 1; i > 0; i--)
                {
                    x = positions[i].x;
                    y = positions[i].y;
                    z = positions[i].z;

                    distancePercentage = Vector3.Distance(positions[i], positions[i - 1]) / totalDistance;
                    durationPercentage = duration * distancePercentage;

                    while (elapsedTime < durationPercentage)
                    {
                        x = function(positions[i].x, positions[i - 1].x, (elapsedTime / durationPercentage));
                        y = function(positions[i].y, positions[i - 1].y, (elapsedTime / durationPercentage));
                        z = function(positions[i].z, positions[i - 1].z, (elapsedTime / durationPercentage));

                        elapsedTime += Time.deltaTime;

                        if (xAxis)
                            x = transform.position.x;
                        if (yAxis)
                            y = transform.position.y;
                        if (zAxis)
                            z = transform.position.z;

                        transform.position = new Vector3(x, y, z);

                        yield return null;
                    }

                    if (!xAxis && !yAxis && !zAxis)
                        transform.position = positions[i - 1];

                    elapsedTime = 0f;

                    if (i > positions.Length + 2)
                        OnNewPosition.Invoke();
                }

                OnEnd.Invoke();

            }
        }
        while (loop);


    }

    public float GetTotalDistance(Vector3[] positions)
    {
        float totalDistance = 0;

        for (int i = 0; i < positions.Length - 1; i++)
            totalDistance += Vector3.Distance(positions[i], positions[i + 1]);

        return totalDistance;
    }

    public void Initialize()
    {
        entity = GetComponent<GridEntity>();
    }
}

public enum GridMovementType
{
    WASD,
    MouseSelection
}

public enum MoveDirection
{
    Up,
    Left,
    Down,
    Right
}