﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Unit : MonoBehaviour
{
    [SerializeField]
    private UnitType type;

    public string Name
    {
        get
        {
           return _name;
        }
        set
        {
            _name = value;
        }
    }
    [SerializeField]
    private string _name;

}

public enum UnitType
{
    Character,
    Enemy
}
