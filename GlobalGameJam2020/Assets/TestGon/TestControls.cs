﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestControls : MonoBehaviour
{
    public InputMaster controls;

    public int ol;

    private void Awake()
    {
        controls.Character1.Shoot.performed += ctx => Shoot();
    }

    public void Shoot()
    {
        Debug.Log("Shoot");
    }

    private void Update()
    {
        Gamepad gp = InputSystem.GetDevice<Gamepad>();
        if (gp.triangleButton.wasPressedThisFrame)
        {
            Debug.Log("Oli");
        }
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }
}
