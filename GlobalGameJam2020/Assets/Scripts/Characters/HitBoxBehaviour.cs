﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.CustomInput;

public class HitBoxBehaviour : MonoBehaviour
{
    [SerializeField]
    CharacterBehaviour character;

    [SerializeField]
    SimpleInput simpleInput;

    [SerializeField]
    private float elapsedTimeToDeflood;
    [SerializeField]
    private float timeThresholdToDeflood;

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetButtonDown(simpleInput.ActionGrab))
        {
            if (other.tag == character.repairBoxesPoolTag)
            {
                if (!character.isGrabbing)
                {
                    character.Grab(other.GetComponent<Pool>().GetPooledObject().gameObject);
                }
            }
            else if (other.tag == character.brokenTileTag)
            {
                if (character.isGrabbing)
                {
                    character.RepairTile(other.GetComponentInParent<ShipTile>());
                }
            }
        }
        if (Input.GetButtonDown(simpleInput.ActionBucket))
        {
            elapsedTimeToDeflood = 0;
            character.imgFillFlood.gameObject.SetActive(true);
            character.imgBackGround.gameObject.SetActive(true);
        }
        if (Input.GetButton(simpleInput.ActionBucket))
        {
            if (other.tag == "RoomCollider")
            {
                elapsedTimeToDeflood += Time.deltaTime;

                character.imgFillFlood.fillAmount = Mathf.Clamp(1 - (elapsedTimeToDeflood / timeThresholdToDeflood), 0, 1);

                if (elapsedTimeToDeflood >= timeThresholdToDeflood)
                {
                    // other.GetComponentInParent<Room>().SwitchState(RoomStates.Healthy);
                    other.GetComponentInParent<Room>().TakeWaterFromRoom();
                    character.imgFillFlood.gameObject.SetActive(false);
                    character.imgBackGround.gameObject.SetActive(false);
                }
            }

            // character.imgFillFlood.gameObject.SetActive(false);
            // character.imgBackGround.gameObject.SetActive(false);
        }
        else if (Input.GetButtonUp(simpleInput.ActionBucket))
        {
            character.imgFillFlood.gameObject.SetActive(false);
            character.imgBackGround.gameObject.SetActive(false);
        }

    }
}