﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.CustomInput;

[RequireComponent(typeof(CharacterController))]
public class SimpleMove : MonoBehaviour
{
    [SerializeField]
    SimpleInput simpleInput;

    [Range(0.1f,5f)]
    public float speed;

    public float gravity = 20f;

    private CharacterController CharCtrl;

    private void Start()
    {
        CharCtrl = this.GetComponent<CharacterController>();
    }

    private void Update()
    {
        Movement();
        Interaction();
    }

    private void Movement()
    {
        Vector3 NextDir = new Vector3(Input.GetAxis(simpleInput.ActionHorizontal) * speed, 0, Input.GetAxis(simpleInput.ActionVertical) * speed);

        if (NextDir != Vector3.zero)
            transform.rotation = Quaternion.LookRotation(NextDir);

        NextDir.y -= gravity * Time.deltaTime;
        CharCtrl.Move(NextDir / 8);
    }

    private void Interaction()
    {
        if (Input.GetButtonDown(simpleInput.ActionGrab))
        {
            Debug.Log($"<b> GRAB </b>");
        }

        if (Input.GetButtonDown(simpleInput.ActionBucket))
        {
            Debug.Log($"<b> BUCKET </b>");
        }
    }

}