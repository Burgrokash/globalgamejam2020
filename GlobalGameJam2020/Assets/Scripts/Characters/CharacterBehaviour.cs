﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.CustomInput;
using UnityEngine.UI;

public class CharacterBehaviour : MonoBehaviour
{
    public SimpleInput simpleInput;
    [Space]

    [SerializeField]
    public bool isGrabbing;

    public string repairBoxesPoolTag;
    public string brokenTileTag;

    [SerializeField]
    private Transform holdPosition;

    [SerializeField]
    private BoxCollider hitBox;

    private RepairBox holdingRepairBox;

    [SerializeField]
    public Image imgFillFlood;
    public Image imgBackGround;
    public TextMeshProUGUI controllerTxt;

    private void Start()
    {
        isGrabbing = false;
        hitBox.gameObject.SetActive(true);

        controllerTxt.text = GetControllerText();

        StartCoroutine(RemoveText());
    }

    public void Update()
    {

    }

    public void Grab(GameObject grabedBox)
    {
        isGrabbing = true;

        Debug.Log("Grab");

        /*
        Vector3[] auxPositions = new Vector3[2];
        auxPositions[0] = grabedBox.transform.position;
        auxPositions[1] = holdPosition.position;

        grabedBox.GetComponent<Pyros.Movement>().StartMove(auxPositions,1f);
        */
        holdingRepairBox = grabedBox.GetComponent<RepairBox>();

        grabedBox.transform.position = holdPosition.position;
        grabedBox.transform.SetParent(this.transform);

        AudioManager.Main.PlayNewSound(AudioClips.AGARRAR.ToString());

    }

    public void RepairTile(ShipTile tileBeingRepaired)
    {
        //TODO: Interpolacion entre la posicion en la que lo agarro y la posicion final.

        tileBeingRepaired.Repair();
        isGrabbing = false;

        holdingRepairBox.gameObject.SetActive(false);
        holdingRepairBox = null;

        AudioManager.Main.PlayNewSound(AudioClips.TAPARHUECO.ToString());
    }

    public void SetTileTransform()
    {

    }

    IEnumerator RemoveText()
    {
        yield return new WaitForSeconds(3f);

        controllerTxt.gameObject.SetActive(false);
    }

    private string GetControllerText()
    {
        switch (simpleInput.inputType)
        {
            case InputType.WASD:
                return "WASD";
            case InputType.Arrows:
                return "ARROWS";
            case InputType.GenericJoystick:
                return "Joystick";
            case InputType.NintendoSwitch:
                return "Joy Con";
        }

        return null;
    }

}