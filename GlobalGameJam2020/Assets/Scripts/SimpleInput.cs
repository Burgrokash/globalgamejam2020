﻿namespace UnityEngine.CustomInput
{
    public enum InputType
    {
        WASD,
        Arrows,
        GenericJoystick,
        NintendoSwitch
    }

    public class SimpleInput : MonoBehaviour
    {
        public InputType inputType;
        
        private string _actionHorizontal;
        private string _actionVertical;
        private string _actionGrab;
        private string _actionBucket;

        public string ActionHorizontal { get { return _actionHorizontal; } }
        public string ActionVertical { get { return _actionVertical; } }
        public string ActionGrab { get { return _actionGrab; } }
        public string ActionBucket { get { return _actionBucket; } }

        private void Start()
        {
            switch (inputType)
            {
                case InputType.WASD:
                    _actionHorizontal = "Horizontal_WASD";
                    _actionVertical = "Vertical_WASD";
                    _actionGrab = "Grab_WASD";
                    _actionBucket = "Bucket_WASD";
                    break;

                case InputType.Arrows:
                    _actionHorizontal = "Horizontal_Arrows";
                    _actionVertical = "Vertical_Arrows";
                    _actionGrab = "Grab_Arrows";
                    _actionBucket = "Bucket_Arrows";
                    break;
                case InputType.GenericJoystick:
                    _actionHorizontal = "Horizontal_GenericJoystick";
                    _actionVertical = "Vertical_GenericJoystick";
                    _actionGrab = "Grab_GenericJoystick";
                    _actionBucket = "Bucket_GenericJoystick";
                    break;

                case InputType.NintendoSwitch:
                    _actionHorizontal = "Horizontal_NintendoSwitch";
                    _actionVertical = "Vertical_NintendoSwitch";
                    _actionGrab = "Grab_NintendoSwitch";
                    _actionBucket = "Bucket_NintendoSwitch";
                    break;

                default:
                    break;
            }

        }

        private void Update()
        {

        }
    }
}