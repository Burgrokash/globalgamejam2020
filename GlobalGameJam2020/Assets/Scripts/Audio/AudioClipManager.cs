﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipManager : MonoBehaviour
{
    
}

public enum AudioClips
{
    AGARRAR,
    CAMINAR,
    ENTRAAGUA,
    INUNDACIÓN,
    OLEAJE,
    SEACABAELTIEMPO,
    SEROMPEELSUELO,
    SOLTARCAJA,
    TAPARHUECO,
    PIRATASOST
}