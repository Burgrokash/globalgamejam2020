﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlaySounds : MonoBehaviour
{
    [ContextMenu("PlayFlood")]
    public void PlayFlood()
    {
        AudioManager.Main.PlayNewSound("INUNDACIÓN");
    }
}
