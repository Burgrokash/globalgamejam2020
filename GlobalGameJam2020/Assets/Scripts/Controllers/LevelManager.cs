﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager control;

    private void Awake()
    {
        if (control == null)
        {
            control = this;
        }
    }

    [SerializeField]
    private LevelPhaces actualPhace;

    [SerializeField]
    private float floodMinTime;
    [SerializeField]
    private float floodMaxTime;

    [SerializeField]
    public float EndGameElapsedTime
    {
        get
        {
            return endGameElapsedTime;
        }
        set
        {
            endGameElapsedTime = value;
            txtTimeToEnd.text = string.Format("{0}:{1:00}", (int)endGameElapsedTime / 60, (int)endGameElapsedTime % 60);
        }
    }
    private float endGameElapsedTime;

    [SerializeField]
    private float endGameTimeThreslhold;

    [SerializeField]
    List<Room> rooms;

    public int FloodedRoms
    {
        get
        {
            return floodedRoms;
        }
        set
        {
            floodedRoms = value;
            if (floodedRoms >= 4)
            {
                Lose();
            }
        }
    }
    [SerializeField]
    private int floodedRoms;
    [Space]

    [SerializeField]
    private TextMeshProUGUI txtTimeToEnd;

    [Space]

    public Transform spawnPoint;

    private float floodThreshold;
    private float elapsedTime;

    private void Start()
    {
        elapsedTime = 0;
        floodThreshold = Random.Range(floodMinTime, floodMaxTime);

        StartCoroutine(HandleFlood());
        PlayTheme();
        StartCoroutine(StartCountDown());
    }

    public void PlayTheme()
    {
        AudioManager.Main.PlayNewSound(AudioClips.PIRATASOST.ToString());
    }

    public void ResetCharacterPosition(CharacterBehaviour character)
    {
        character.transform.position = spawnPoint.position;
    }

    IEnumerator HandleFlood()
    {
        while (true)
        {
            if (elapsedTime < floodThreshold)
            {
                // Todavia no hay que inundar
                elapsedTime += Time.deltaTime;
            } else
            {
                // Hay que inundar

                // Reiniciar contador
                elapsedTime = 0;
                floodThreshold = Random.Range(floodMinTime, floodMaxTime);

                // Elegir room al azar y romper tile
                List<Room> healthyRooms = rooms.FindAll(r => r.GetState() == RoomStates.Healthy);
                if (healthyRooms.Count > 0)
                {
                    int randomRoom = Random.Range(0, healthyRooms.Count);
                    Room room = healthyRooms[randomRoom];
                    room.FloodRandomTile();
                }
            }
            yield return null;
        }
    }

    /// <summary>
    /// Called when you win the level.
    /// </summary>
    public void Win()
    {
        Debug.Log("Hey Cmon, u manage to stay afloat the whole time");
    }

    /// <summary>
    /// Called when you Lose.
    /// </summary>
    public void Lose()
    {
        SceneManager.LoadScene(0);
    }

    public void StartNewFace(LevelPhaces newPhace)
    {
        switch (newPhace)
        {
            //TODO: Todos estos valores podrian estar dentro de un scriptable object.
            // Tambien se puede agregar logica o sonidos aca.

            case LevelPhaces.InitialPhace:
                Debug.Log("InitialPhace");
                actualPhace = LevelPhaces.InitialPhace;
                floodMinTime = 5f;
                floodMaxTime = 10f;
                break;
            case LevelPhaces.MediumPhace:
                Debug.Log("MediumPhace");
                actualPhace = LevelPhaces.MediumPhace;
                floodMinTime = 3f;
                floodMaxTime = 7f;
                break;
            case LevelPhaces.LastPhace:
                Debug.Log("FinalPhace");
                actualPhace = LevelPhaces.LastPhace;
                floodMinTime = 1.5f;
                floodMaxTime = 3.5f;
                break;
            default:
                break;
        }
    }

    IEnumerator StartCountDown()
    {
        EndGameElapsedTime = endGameTimeThreslhold;

        float mediumPhaceTime = (endGameTimeThreslhold / 3f) * 2;
        float lastPhaceTime = endGameTimeThreslhold / 3f;

        while (EndGameElapsedTime >= 0f)
        {
            if (EndGameElapsedTime < mediumPhaceTime && actualPhace != LevelPhaces.MediumPhace)
            {
                StartNewFace(LevelPhaces.MediumPhace);
            }
            if (EndGameElapsedTime < lastPhaceTime && actualPhace != LevelPhaces.LastPhace)
            {
                StartNewFace(LevelPhaces.LastPhace);
            }

            EndGameElapsedTime -= Time.deltaTime;
            yield return null;
        }
        Win();
    }

    public enum LevelPhaces
    {
        InitialPhace,
        MediumPhace,
        LastPhace
    }
}

