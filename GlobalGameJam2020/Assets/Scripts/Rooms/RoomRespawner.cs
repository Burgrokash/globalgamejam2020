﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomRespawner : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Character")
        {
            Debug.Log("There is a character there");
            Debug.Log("The other name is: " + other.gameObject.name);
            LevelManager.control.ResetCharacterPosition(other.GetComponent<CharacterBehaviour>());
        }
    }
}
