﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pyros;

public class Room : MonoBehaviour
{
    [SerializeField]
    private RoomStates state;

    [SerializeField]
    private int brokenTiles;

    [SerializeField]
    private float elapsedTimeWhenJustDeflooded;

    /// <summary>
    /// This is actually the safe time untill the tiles from the room can get broken again;
    /// </summary>
    [SerializeField]
    private float timeThresholdWhenJustDeflooded;

    [SerializeField]
    private float elapsedTimeToDestroy;

    [SerializeField]
    private float timeThresholdToDestroy;

    /// <summary>
    /// This is a GameObject really.
    /// </summary>
    [SerializeField]
    private Collider[] roomCollider;

    [SerializeField]
    private Movement movement;

    [SerializeField]
    private Transform waterStartPosition;

    [SerializeField]
    private Transform waterEndPosition;

    private void Awake()
    {
        //TODO: Tomar el collider del room por codigo, sino hacer un prefab de cada room
        UpdateRoomColliders(false);
    }

    public void UpdateRoomColliders(bool isActive)
    {
        for (int i = 0; i < roomCollider.Length; i++)
        {
            roomCollider[i].gameObject.SetActive(isActive);
        }
    }

    public void SwitchState(RoomStates newState)
    {
        state = newState;

        switch (state)
        {
            case RoomStates.Healthy:
                StartCoroutine(WaitWhenJustDeflooded());
                Vector3[] aux1 = new Vector3[2];
                aux1[1] = waterStartPosition.position;
                aux1[0] = waterEndPosition.position;
                movement.StartMove(aux1, 1f);
                LevelManager.control.FloodedRoms--;
                UpdateRoomColliders(false);
                break;
            case RoomStates.Flooded:
                Vector3[] aux2 = new Vector3[2];
                aux2[0] = waterStartPosition.position;
                aux2[1] = waterEndPosition.position;
                movement.StartMove(aux2, 1f);
                LevelManager.control.FloodedRoms++;
                UpdateRoomColliders(true);
                break;
            case RoomStates.Destroyed:
                UpdateRoomColliders(true);
                break;
            default:
                break;
        }

    }

    public void AddBrokenTile()
    {
        brokenTiles++;
        if (brokenTiles >= 4)
        {
            SwitchState(RoomStates.Flooded);

            StartCoroutine(RunTimerToDestroy());
            //TODO: Activar que la room es incaminable y sacar al character de la room
        }
    }

    public void RemoveBrokenTile()
    {
        brokenTiles--;
    }

    public void FloodRandomTile()
    {
        if (state == RoomStates.JustDeflooded)
            return;

        ShipTile[] tiles = transform.GetComponentsInChildren<ShipTile>();
        List<ShipTile> healthyTiles = new List<ShipTile>();
        
        for (int i = 0; i < tiles.Length; i++)
        {
            // TODO: Seleccionar una tile que no tenga el personaje encima
            if (tiles[i].GetState() != ShipTileStates.Broken && tiles[i].GetState() != ShipTileStates.Unwalkable)
            {
                healthyTiles.Add(tiles[i]);
            }
        }

        while (healthyTiles.Count > 0)
        {
            int randomTile = Random.Range(0, tiles.Length);
            if (tiles[randomTile].GetState() != ShipTileStates.Broken)
            {
                tiles[randomTile].Broke();
                break;
            }
        }

    }

    [ContextMenu("Take Water Away")]
    public void TakeWaterFromRoom()
    {
        SwitchState(RoomStates.Healthy);
    }

    [ContextMenu("Flood Room")]
    public void TestFloodRoom()
    {
        SwitchState(RoomStates.Flooded);
    }

    IEnumerator RunTimerToDestroy()
    {
        elapsedTimeToDestroy = 0;
        while (elapsedTimeToDestroy <= timeThresholdToDestroy && state == RoomStates.Flooded)
        {
            elapsedTimeToDestroy += Time.deltaTime;
            yield return null;
        }
        if (elapsedTimeToDestroy > timeThresholdToDestroy)
        {
            SwitchState(RoomStates.Destroyed);
        }
    }

    IEnumerator WaitWhenJustDeflooded()
    {
        state = RoomStates.JustDeflooded;

        elapsedTimeWhenJustDeflooded = 0;
        while (elapsedTimeWhenJustDeflooded <= timeThresholdWhenJustDeflooded)
        {
            elapsedTimeWhenJustDeflooded += Time.deltaTime;
            yield return null;
        }

        state = RoomStates.Healthy;
    }

    public RoomStates GetState()
    {
        return state;
    }
}

public enum RoomStates
{
    Healthy,
    Flooded,
    Destroyed,
    JustDeflooded
}