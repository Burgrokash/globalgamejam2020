﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipTile : MonoBehaviour
{
    [SerializeField]
    private ShipTileStates state;

    /// <summary>
    /// 0 is Healthy, 1 is Broken, 2 is Repaired.
    /// </summary>
    [SerializeField]
    private Transform[] tilesGFX;

    public void Start()
    {
        SwitchGraphics(ShipTileStates.Healthy);
    }

    public ShipTileStates GetState()
    {
        return state;
    }

    [ContextMenu("Broke")]
    public void Broke()
    {
        state = ShipTileStates.Broken;
        SwitchGraphics(state);
        transform.parent.GetComponent<Room>().AddBrokenTile();
    }

    [ContextMenu("Repair")]
    public void Repair()
    {
        state = ShipTileStates.Repaired;
        SwitchGraphics(state);
        transform.parent.GetComponent<Room>().RemoveBrokenTile();
    }

    public void SwitchGraphics(ShipTileStates state)
    {
        //TODO: Hacer un estado de Unwalkable que inicie un gameobject que tiene un collider enorme.

        switch (state)
        {
            case ShipTileStates.Healthy:
                tilesGFX[0].gameObject.SetActive(true);
                tilesGFX[1].gameObject.SetActive(false);
                tilesGFX[2].gameObject.SetActive(false);
                break;
            case ShipTileStates.Broken:
                AudioManager.Main.PlayNewSound(AudioClips.SEROMPEELSUELO.ToString());
                tilesGFX[0].gameObject.SetActive(false);
                tilesGFX[1].gameObject.SetActive(true);
                tilesGFX[2].gameObject.SetActive(false);
                break;
            case ShipTileStates.Repaired:
                tilesGFX[0].gameObject.SetActive(false);
                tilesGFX[1].gameObject.SetActive(false);
                tilesGFX[2].gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }

}

public enum ShipTileStates
{
    Healthy,
    Broken,
    Repaired,
    Unwalkable
}